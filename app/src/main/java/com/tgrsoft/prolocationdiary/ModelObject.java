package com.tgrsoft.prolocationdiary;

public enum ModelObject {

    RCMD(R.string.title_rcmd, R.layout.activity_main_rcmd),
    MINE(R.string.title_food, R.layout.main_view_food),
    FOOD(R.string.title_travel, R.layout.activity_main_rcmd);

    private int mTitleResId;
    private int mLayoutResId;

    ModelObject(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}