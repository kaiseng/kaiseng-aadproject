package com.tgrsoft.prolocationdiary;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.tgrsoft.prolocationdiary.Account.AccountActivity;
import com.tgrsoft.prolocationdiary.AuthView.AuthSignInActivity;
import com.tgrsoft.prolocationdiary.AuthView.AuthSignUpActivity;
import com.tgrsoft.prolocationdiary.Components.AsyncTaskLoadImage;
import com.tgrsoft.prolocationdiary.MainView.MapsActivity;
import com.tgrsoft.prolocationdiary.MainView.RecommendActivity;

public class MainActivity extends AppCompatActivity  {

    private FirebaseAuth mAuth;
    HorizontalScrollView tabbar;
    private FusedLocationProviderClient mFusedLocationClient;
    FragmentPagerAdapter adapterViewPager;
    TextView textView;
    private Button signOutBtn;

    Button tab0;
    Button tab1;
    Button tab2;


    ListView list;
    String[] web = {
            "Food Diary example 1",
            "Travel Diary example 1",
            "Food Diary example 2",
            "Travel Diary example 2",
            "Best Recommend Diary example"
    } ;
    Integer[] imageId = {
            R.drawable.fd,
            R.drawable.tb,
            R.drawable.fd3,
            R.drawable.td,
            R.drawable.pic

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.get_location);
        mAuth = FirebaseAuth.getInstance();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        // hide status and nav bottom
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

        // Main scroll views
        CustomList adapter = new
                CustomList(MainActivity.this, web, imageId);
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Toast.makeText(MainActivity.this, "You had press the " +web[+ position], Toast.LENGTH_SHORT).show();

            }

        });

        final ViewPager viewPager = (ViewPager) findViewById(R.id.mainList);
        //viewPager.setAdapter(new CustomPagerAdapter(this));
        adapterViewPager = new MyPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapterViewPager);
        // buttons
        Button openWriteBtn = findViewById(R.id.write);

        final TextView get_location = findViewById(R.id.get_location);

        signOutBtn = findViewById(R.id.signout);
        signOutBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(MainActivity.this, AuthSignInActivity.class);
                finishAffinity();
                startActivity(intent);
            }
        });

        // horizon tab view
        tabbar = findViewById(R.id.tabbar);
        final int numofitemsintabbar = tabbar.getChildCount();
        tab0 = (Button) findViewById(R.id.tabitem_0);
        tab1 = (Button) findViewById(R.id.tabitem_1);
        tab2 = (Button) findViewById(R.id.tabitem_2);

        // title of the view
        final TextView title = findViewById(R.id.view_title);

        /*
        Buttons onclick listeners
         */

        // on click to open the write diary button
        textView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MapsActivity.class));
                overridePendingTransition(R.anim.bottom_to_up, R.anim.up_to_bottom);
            }
        });

        openWriteBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, WriteActivity.class));
                overridePendingTransition(R.anim.bottom_to_up, R.anim.up_to_bottom);
            }
        });


        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    0);
        }


        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {

                            get_location.setText(location.getProvider());
                            Log.e("dd",location.getProvider());
                        }
                    }
                });


        // on swipe the pages
        tab0.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                viewPager.setCurrentItem(0);
            }
        });
        tab1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                viewPager.setCurrentItem(1);
            }
        });
        tab2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                viewPager.setCurrentItem(2);
            }
        });
        //initViewPager();
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int page, float per, int positionOffsetPixels) {
                //Log.e("---","value "+ per);
                float half = 0.3f;
                if (per != 0){
                    if (page == 0) {
                        tab0.setAlpha(half + (1f-half) * (1- per));
                        //tab0.setLayoutParams(new LinearLayout.LayoutParams(100,100));
                        tab1.setAlpha(half + (1f-half) * per);
                        //tab1.setLayoutParams(new LinearLayout.LayoutParams(80,80));
                        tab2.setAlpha(half);
                        //tab2.setLayoutParams(new LinearLayout.LayoutParams(80,80));
                    } else if (page == 1){
                        tab0.setAlpha(half);
                        //tab0.setLayoutParams(new LinearLayout.LayoutParams(80,80));
                        tab1.setAlpha(half + (1f-half) * (1- per));
                        //tab1.setLayoutParams(new LinearLayout.LayoutParams(100,100));
                        tab2.setAlpha(half + (1f-half) * per);
                        //tab2.setLayoutParams(new LinearLayout.LayoutParams(80,80));
                    } else if (page == 2){
                        tab0.setAlpha(half + (1f-half) * per);
                        //tab0.setLayoutParams(new LinearLayout.LayoutParams(80,80));
                        tab1.setAlpha(half);
                        //tab1.setLayoutParams(new LinearLayout.LayoutParams(80,80));
                        tab2.setAlpha(half + (1f-half) * (1- per));
                        //tab2.setLayoutParams(new LinearLayout.LayoutParams(100,100));
                    }
                } else {
                    if (page == 0) {
                        title.setText(R.string.title_rcmd);
                        tab0.setAlpha(1);
                        //tab0.setLayoutParams(new LinearLayout.LayoutParams(100,100));
                        tab1.setAlpha(half);
                        //tab1.setLayoutParams(new LinearLayout.LayoutParams(80,80));
                        tab2.setAlpha(half);
                        //tab2.setLayoutParams(new LinearLayout.LayoutParams(80,80));
                    } else if (page == 1){
                        title.setText(R.string.title_food);
                        tab0.setAlpha(half);
                        //tab0.setLayoutParams(new LinearLayout.LayoutParams(80,80));
                        tab1.setAlpha(1);
                        //tab1.setLayoutParams(new LinearLayout.LayoutParams(100,100));
                        tab2.setAlpha(half);
                        //tab2.setLayoutParams(new LinearLayout.LayoutParams(80,80));
                    } else if (page == 2){
                        title.setText(R.string.title_travel);
                        tab0.setAlpha(half);
                        //tab0.setLayoutParams(new LinearLayout.LayoutParams(80,80));
                        tab1.setAlpha(half);
                        //tab1.setLayoutParams(new LinearLayout.LayoutParams(80,80));
                        tab2.setAlpha(1);
                        //tab2.setLayoutParams(new LinearLayout.LayoutParams(100,100));
                    } else if (page == 3){
                        title.setText(R.string.title_map);
                    }
                }
            }

            @Override
            public void onPageSelected(int page) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }
}

class MyPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 3;

    public MyPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return RecommendActivity.newInstance(0, "Page # 1");
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return RecommendActivity.newInstance(1, "Page # 2");
            case 2: // Fragment # 1 - This will show SecondFragment
                return RecommendActivity.newInstance(2, "Page # 3");
            default:
                return null;
        }
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

}


