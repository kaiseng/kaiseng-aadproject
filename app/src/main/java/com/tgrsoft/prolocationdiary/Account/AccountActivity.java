package com.tgrsoft.prolocationdiary.Account;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.tgrsoft.prolocationdiary.AuthView.AuthSignInActivity;
import com.tgrsoft.prolocationdiary.MainActivity;
import com.tgrsoft.prolocationdiary.R;
import com.tgrsoft.prolocationdiary.WriteActivity;

public class AccountActivity extends AppCompatActivity {

    private TextView username;
    private TextView location;
    private Button backToMain,signOutBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        backToMain = findViewById(R.id.backtomain1);
        backToMain.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(AccountActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.right_to_left_back, R.anim.left_to_right_back);

            }
        });

        signOutBtn = findViewById(R.id.signout);
        signOutBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(AccountActivity.this, AuthSignInActivity.class);
                finishAffinity();
                startActivity(intent);
            }
        });
    }



}
