package com.tgrsoft.prolocationdiary.MainView;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.tgrsoft.prolocationdiary.Components.AsyncTaskLoadImage;
import com.tgrsoft.prolocationdiary.MainActivity;
import com.tgrsoft.prolocationdiary.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecommendActivity extends Fragment {


    private String title;
    private int page;
    View view;
    private List<ItemData> rcmdList;
    GridView listView;
    ItemAdapter adapter;
    // newInstance constructor for creating fragment with arguments
    public static RecommendActivity newInstance(int page, String title) {
        RecommendActivity fragmentFirst = new RecommendActivity();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_main_rcmd, container, false);




        return view;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);

        Log.e("t","works");
        rcmdList = new ArrayList<ItemData>();
        getRecommendData();
        adapter = new ItemAdapter(getActivity(), R.layout.activity_main_rcmd_item, rcmdList);
        listView = getActivity().findViewById(R.id.mainRcmd);
        listView.setAdapter(adapter); //use to display all below information//


    }
    // Load list of Recommend
    public void getRecommendData(){
    ItemData apple = new ItemData("account.png","This is the recommend diary."); // 图片id
    rcmdList.add(apple); // 苹果增加到链表
    ItemData apple1 = new ItemData("calendar.png","This is the travel diary" ); // 图片id
    rcmdList.add(apple1); // 苹果增加到链表
    ItemData apple2 = new ItemData("chart.png","This is the food diary"); // 图片id
    rcmdList.add(apple2); // 苹果增加到链表
    ItemData apple3 = new ItemData("contacts.png","This is the best travel diary"); // 图片id
    rcmdList.add(apple3); // 苹果增加到链表
    ItemData apple4 = new ItemData("diet.png","This is the best food diary"); // 图片id
    rcmdList.add(apple4); // 苹果增加到链表
    ItemData apple5 = new ItemData("fries.png","Food bar diary"); // 图片id
    rcmdList.add(apple5); // 苹果增加到链表
}


}