package com.tgrsoft.prolocationdiary.MainView;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.tgrsoft.prolocationdiary.Components.AsyncTaskLoadImage;

public class ItemData {

    private String imagePath;
    private String Content;


    // declare the image and content
    public ItemData(String imagePath,String Content){
        this.imagePath = imagePath;
        this.Content = Content;
    }

    // get the image and content
    public String getImagePath() {
        return imagePath;
    }

    public String getContent() {
        return Content;
    }


}

