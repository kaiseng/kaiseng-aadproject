package com.tgrsoft.prolocationdiary.MainView;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.tgrsoft.prolocationdiary.Components.AsyncTaskLoadImage;
import com.tgrsoft.prolocationdiary.R;

import java.util.List;

public class ItemAdapter extends ArrayAdapter<ItemData> {


    // sub item layout id
    private int resourceId;

    public ItemAdapter(Context context,         // context上下文
                       int textViewResourceId,  // 子项布局的id
                       List<ItemData> objects){    // 数据链表
        super(context, textViewResourceId, objects);
        resourceId = textViewResourceId;
    }

    // overwrite getView
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemData item = getItem(position);
        View view;
        ViewHolder viewHolder;

        if (convertView == null){
            view = LayoutInflater.from(getContext()).inflate(resourceId, null);
            viewHolder = new ViewHolder();
            viewHolder.itemImage = (ImageView) view.findViewById(R.id.rcmd_item_img);
            viewHolder.itemContent = (TextView) view.findViewById(R.id.rcmd_item_content);
            view.setTag(viewHolder);
        }else{
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }


        LoadImage(item.getImagePath(),viewHolder.itemImage);
        viewHolder.itemContent.setText(item.getContent());

        return view;
    }
    private void LoadImage(String path, final ImageView testImg){

        FirebaseStorage storage = FirebaseStorage.getInstance("gs://diary-bc1a3.appspot.com");
        StorageReference storageRef = storage.getReference();
        storageRef.child(path).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(final Uri uri) {

                new AsyncTaskLoadImage(testImg).execute(uri.toString());

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
            }
        });
    }

    class ViewHolder{
        ImageView itemImage;
        TextView itemContent;
    }
}